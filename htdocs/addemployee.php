<?
require_once 'includes/security.php';
?>
<html>
	<head>
		<script src="static/jquery-3.4.1.js"></script>
		<script src="static/validation.js"></script>
		<link rel="stylesheet" href="css/styles.css">
	</head>
	<body>
		<div id="page">
			<div id="header">
				<?php require_once 'includes/header.php'; ?>
			</div>
			<div id="content">
				<div id="leftpanel">
					<?php require_once 'includes/leftpanel.php'; ?>
				</div>
				<div id="body">
					<div>
						<div class="pageheading">Add Employee</div>
						<div class="msg"><? isset($_GET['msg']) ? print $_GET['msg'] : "" ?></div>
						<div class="msg">*(Required)</div>
						<form method="post" action="addemployeeaction.php" id="frmaddemployee" onsubmit="return validate(this);" enctype="multipart/form-data">
							<div>
								<div class="formrow">
									<div class="label">Name*</div>
									<div class="element"><input type="text" name="txtname" id="txtname" value=""></div>
								</div>
								<div class="formrow">
									<div class="label">Picture*</div>
									<div class="element"><input type="file" name="flpicture" id="flpicture"></div>
								</div>
								<div class="formrow">
									<div class="label">Address*</div>
									<div class="element"><textarea name="txtaddress" id="txtaddress" value=""></textarea></div>
								</div>
								<div class="formrow">
									<div class="label">Salary*</div>
									<div class="element"><input type="text" name="txtsalary" id="txtsalary" value=""></div>
								</div>
								<div class="formrow">
									<div class="label">Login Info</div>
								</div>
								<div class="formrow">
									<div class="label">Email</div>
									<div class="element"><input type="text" name="txtemail" id="txtemail" value=""></div>
								</div>
								<div class="formrow">
									<div class="label">Password*</div>
									<div class="element"><input type="password" name="txtpassword" id="txtpassword" value=""></div>
								</div>
								<div class="formrow">
									<div class="label"></div>
									<div class="element"><input type="submit" name="btnsubmit" id="btnsubmit" value="Add"></div>
								</div>
							</div>					
						</form>	
					</div>
				</div>
			</div>
			<div id="footer">
				<?php require_once 'includes/footer.php'; ?>
			</div>
		</div>
	</body>
</html>
