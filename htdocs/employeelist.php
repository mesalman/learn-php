<?
require_once 'includes/security.php';
require_once 'includes/dbconnection.php';
?>
<html>
	<head>
		<link rel="stylesheet" href="css/styles.css">
	</head>
	<body>
		<div id="page">
			<div id="header">
				<?php require_once 'includes/header.php'; ?>
			</div>
			<div id="content">
				<div id="leftpanel">
					<?php require_once 'includes/leftpanel.php'; ?>
				</div>
				<div id="body">
					<div class="pageheading">Employees List
						<div style="float:right"><a href="addemployee.php">Add Employee</a></div>
					</div>
					<div class="msg"><? isset($_GET['msg']) ? print $_GET['msg'] : "" ?></div>
					<form method="post" name="frm" action="deleteemployeeaction.php">
					<table width="100%" >
						<tr class="tableheading">
							<td></td>
							<td>ID</td>
							<td></td>
							<td>Name</td>
							<td>Address</td>
							<td>Salary</td>
							<td></td>
						</tr>
						<?
						$rs = mysqli_query($dbconnection, "select * from employees");
						if($rs && mysqli_num_rows($rs) > 0){
							while($row = mysqli_fetch_assoc($rs)){
						?>
							<tr>
								<td><input type="checkbox" name="chkid[]" id="chkid" value="<?=$row["id"];?>"></td>
								<td><?=$row["id"];?></td>
								<td><img src="viewpicture.php?id=<?=$row["id"];?>" height="50px"></td>
								<td><?=$row["name"];?></td>
								<td><?=$row["address"];?></td>
								<td><?=$row["salary"];?></td>
								<td><a href="editemployee.php?id=<?=$row["id"];?>">Edit</a></td>
							</tr>
						<?
							}
						?>
							<tr><td colspan="5"><input type="submit" value="Delete Selected" name="delete" onclick="return confirm('Are you sure?');"></td></tr>
						<?	
						}
						else
						{
						?>	
							<tr>
								<td colspan=4>No record found</td>
							</tr>

						<?
						}
						?>
					</table>
					</form>
				</div>
			</div>
			<div id="footer">
				<?php require_once 'includes/footer.php'; ?>
			</div>
		</div>
	</body>
</html>
